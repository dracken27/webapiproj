﻿using System.Collections.Generic;
using AutoMapper;
using Data;
using Domain;
using Manager.Model;

namespace Manager.Service
{
    public class ProjectService
    {
        private readonly ProjectRepository _projectRepo;
        private readonly ActionAreaRepository _actionAreaRepo;
        private readonly UnitOfWork _unitOfWork;

        public ProjectService(
            ProjectRepository projectRepo,
            ActionAreaRepository actionAreaRepo,
            UnitOfWork unitOfWork)
        {
            _projectRepo = projectRepo;
            _actionAreaRepo = actionAreaRepo;
            _unitOfWork = unitOfWork;
        }

        public List<ProjectInfo> GetAllProjects()
        {
            return Mapper.Map<List<ProjectInfo>>(_projectRepo.GetAll());
        }

        public ProjectInfo GetProjectById(int id)
        {
            return Mapper.Map<ProjectInfo>(_projectRepo.GetById(id));
        }

        public void AddNewProject(ProjectInput newProjectInput)
        {
            var newProject = Mapper.Map<Project>(newProjectInput);
            newProject.Areas = new List<ActionArea>();

            ServiceHelper.UpdateRelatedEntities<ActionArea>(newProject.Areas, newProjectInput.AreaIds, _actionAreaRepo.GetById);

            _projectRepo.Add(newProject);
            _unitOfWork.Comit();
        }

        public void UpdateProject(ProjectInput projectInput)
        {
            if (!projectInput.ProjectId.HasValue) return;

            var dbProj = _projectRepo.GetById(projectInput.ProjectId.Value);
            Mapper.Map(projectInput, dbProj);

            ServiceHelper.UpdateRelatedEntities<ActionArea>(dbProj.Areas, projectInput.AreaIds, _actionAreaRepo.GetById);

            _projectRepo.Update(dbProj);
            _unitOfWork.Comit();
        }
    }
}
