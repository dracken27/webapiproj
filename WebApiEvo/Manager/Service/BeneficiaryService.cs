﻿using System.Collections.Generic;
using Data;
using Domain;
using Manager.Model;

namespace Manager.Service
{
    public class BeneficiaryService
    {
        private readonly BeneficiaryRepository _beneficiaryRepository;
        private readonly JobSectorRepository _jobSectorRepository;
        private readonly UnitOfWork _unitOfWork;

        public BeneficiaryService(
            BeneficiaryRepository beneficiaryRepository,
            JobSectorRepository jobSectorRepository,
            UnitOfWork unitOfWork)
        {
            _beneficiaryRepository = beneficiaryRepository;
            _jobSectorRepository = jobSectorRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<BeneficiaryInfo> GetAllBeneficiaries()
        {
            var all = _beneficiaryRepository.GetAll();
            return AutoMapper.Mapper.Map<IList<BeneficiaryInfo>>(all);
        }

        public BeneficiaryInfo GetBeneficiaryById(int id)
        {
            var beneficiary = _beneficiaryRepository.GetById(id);
            return AutoMapper.Mapper.Map<BeneficiaryInfo>(beneficiary);
        }

        public void AddBeneficiary(BeneficiaryInput beneficiaryInput)
        {
            var beneficiary = AutoMapper.Mapper.Map<Beneficiary>(beneficiaryInput);
            beneficiary.JobSectors = new List<JobSectorLookup>();

            ServiceHelper.UpdateRelatedEntities<JobSectorLookup>(beneficiary.JobSectors, beneficiaryInput.JobSectorIds, _jobSectorRepository.GetById);

            _beneficiaryRepository.Add(beneficiary);
            _unitOfWork.Comit();
        }

        public void UpdateBeneficiary(BeneficiaryInput beneficiaryInput)
        {
            var beneficiary = _beneficiaryRepository.GetById(beneficiaryInput.BeneficiaryId);
            AutoMapper.Mapper.Map(beneficiaryInput, beneficiary);

            ServiceHelper.UpdateRelatedEntities<JobSectorLookup>(beneficiary.JobSectors, beneficiaryInput.JobSectorIds, _jobSectorRepository.GetById);

            _beneficiaryRepository.Update(beneficiary);
            _unitOfWork.Comit();
        }

        public bool DeleteBeneficiary(int id)
        {
            var beneficiary = _beneficiaryRepository.GetById(id);

            if (beneficiary == null)
                return false;

            _beneficiaryRepository.Delete(beneficiary);

            _unitOfWork.Comit();

            return true;
        }
    }
}
