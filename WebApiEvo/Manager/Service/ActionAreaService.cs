﻿using AutoMapper;
using Data;
using Domain;
using Manager.Model;
using System.Collections.Generic;
using System.Linq;

namespace Manager.Service
{
    public class ActionAreaService
    {
        private readonly ProjectRepository _projectRepo;
        private readonly ActionAreaRepository _actionAreaRepo;
        private readonly UnitOfWork _unitOfWork;

        public ActionAreaService(
            ActionAreaRepository actionAreaRepo,
            ProjectRepository projectRepo,
            UnitOfWork unitOfWork)
        {
            _actionAreaRepo = actionAreaRepo;
            _projectRepo = projectRepo;
            _unitOfWork = unitOfWork;
        }

        public List<ActionAreaInfo> GetAreas(int? projectId = null)
        {
            var dbActionAreas = _actionAreaRepo.GetAll();

            if (projectId != null)
            {
                dbActionAreas = dbActionAreas.Where(a => a.Projects.Any(p => p.ProjectId == projectId)).ToList();
            }

            return Mapper.Map<List<ActionAreaInfo>>(dbActionAreas);
        }

        public void AddNewActionArea(ActionAreaInput actionAreaInput)
        {
            var newActionArea = Mapper.Map<ActionArea>(actionAreaInput);
            newActionArea.Projects = new List<Project>();

            ServiceHelper.UpdateRelatedEntities<Project>(newActionArea.Projects, actionAreaInput.ProjectIds, _projectRepo.GetById);

            _actionAreaRepo.Add(newActionArea);
            _unitOfWork.Comit();
        }

        public void UpdateExistingActionArea(ActionAreaInput actionAreaInput)
        {
            if (!actionAreaInput.ActionAreaId.HasValue) return;

            var dbActionArea = _actionAreaRepo.GetById(actionAreaInput.ActionAreaId.Value);
            Mapper.Map(actionAreaInput, dbActionArea);

            ServiceHelper.UpdateRelatedEntities<Project>(dbActionArea.Projects, actionAreaInput.ProjectIds, _projectRepo.GetById);

            _actionAreaRepo.Update(dbActionArea);
            _unitOfWork.Comit();
        }
    }
}
