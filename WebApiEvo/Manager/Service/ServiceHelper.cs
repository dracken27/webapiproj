﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Manager.Service
{
    public static class ServiceHelper
    {
        public static void UpdateRelatedEntities<T>(
            ICollection<T> entities,
            IEnumerable<int> relatedIds,
            Func<int, T> findById)
        {
            if (relatedIds != null)
            {
                entities.Clear();

                foreach (var entityId in relatedIds)
                {
                    var entity = findById(entityId);
                    if (entity == null) continue;

                    entities.Add(entity);
                }
            }
        }
    }
}
