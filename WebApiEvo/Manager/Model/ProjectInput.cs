﻿using System;
using System.Collections.Generic;

namespace Manager.Model
{
    public class ProjectInput
    {
        public int? ProjectId { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Domain.Theme ProjTheme { get; set; }

        public SectorInput ProjSector { get; set; }

        public ICollection<int> AreaIds { get; set; }
    }
}
