﻿using System.Collections.Generic;

namespace Manager.Model
{
    public class ActionAreaInput
    {
        public int? ActionAreaId { get; set; }

        public string CodeName { get; set; }

        public ICollection<int> ProjectIds { get; set; }
    }
}
