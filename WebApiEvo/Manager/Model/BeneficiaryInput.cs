﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain;

namespace Manager.Model
{
    public class BeneficiaryInput
    {
        public int BeneficiaryId { get; set; }
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public AddressInput Address { get; set; }
        public int SexId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public ProfileStatus ProfileStatus { get; set; }
        public IEnumerable<int> JobSectorIds { get; set; }
    }
}
