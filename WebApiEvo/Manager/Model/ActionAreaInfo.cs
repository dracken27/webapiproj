﻿using System.Collections.Generic;

namespace Manager.Model
{
    public class ActionAreaInfo
    {
        public int ActionAreaId { get; set; }

        public string CodeName { get; set; }

        public ICollection<ProjectAreaInfo> Projects { get; set; }
    }
}
