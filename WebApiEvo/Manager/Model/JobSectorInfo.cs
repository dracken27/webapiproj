﻿namespace Manager.Model
{
    public class JobSectorInfo
    {
        public int JobSectorId { get; set; }
        public string JobSector { get; set; }
    }
}
