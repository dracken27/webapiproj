﻿using System;
using System.Collections.Generic;
using Domain;

namespace Manager.Model
{
    public class BeneficiaryInfo
    {
        public int BeneficiaryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SexLookup Sex { get; set; }
        public Address Address { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public ProfileStatus ProfileStatus { get; set; }
        public IEnumerable<JobSectorInfo> JobSectors { get; set; }
    }
}