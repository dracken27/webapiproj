﻿namespace Manager.Model
{
    public class SectorInfo
    {
        public int SectorId { get; set; }

        public string Name { get; set; }
    }
}
