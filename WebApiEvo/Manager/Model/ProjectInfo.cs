﻿using System;
using System.Collections.Generic;

namespace Manager.Model
{
    public class ProjectInfo
    {
        public int ProjectId { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }

        public Domain.Theme ProjTheme { get; set; }

        public SectorInfo ProjSector { get; set; }

        public ICollection<ActionAreaProjectInfo> Areas { get; set; }
    }
}
