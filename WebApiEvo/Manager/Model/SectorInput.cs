﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Manager.Model
{
    public class SectorInput
    {
        public int SectorId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
