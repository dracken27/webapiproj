﻿namespace Manager.Model
{
    public class AddressInput
    {
        public int? AddressId { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
    }
}
