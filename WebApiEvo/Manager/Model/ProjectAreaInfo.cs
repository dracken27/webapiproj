﻿using System;

namespace Manager.Model
{
    public class ProjectAreaInfo
    {
        public int ProjectId { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Domain.Theme ProjTheme { get; set; }

        public SectorInfo ProjSector { get; set; }
    }
}
