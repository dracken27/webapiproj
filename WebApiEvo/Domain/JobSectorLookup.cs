﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class JobSectorLookup
    {
        [Key]
        public int JobSectorId { get; set; }
        public string JobSector { get; set; }

        public virtual ICollection<Beneficiary> Beneficiaries { get; set; }
    }
}
