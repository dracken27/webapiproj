﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Project
    {
        public int ProjectId { get; set; }

        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public Theme ProjTheme { get; set; }

        public virtual Sector ProjSector { get; set; }

        public virtual ICollection<Beneficiary> Beneficiaries { get; set; }

        public virtual ICollection<ActionArea> Areas { get; set; }
    }
}
