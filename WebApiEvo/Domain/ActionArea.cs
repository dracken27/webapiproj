﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class ActionArea
    {
        public int ActionAreaId { get; set; }

        public string CodeName { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
