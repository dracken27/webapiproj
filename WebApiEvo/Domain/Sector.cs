﻿using System.Collections.Generic;

namespace Domain
{
    public class Sector
    {
        public int SectorId { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Project> Projects { get; set; }
    }
}
