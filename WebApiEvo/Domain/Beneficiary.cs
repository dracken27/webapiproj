﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain
{
    public class Beneficiary
    {
        public int BeneficiaryId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public ProfileStatus ProfileStatus { get; set; }
        public Address Address { get; set; }
        public virtual Project Project { get; set; }

        public int SexId { get; set; }
        [ForeignKey("SexId")]
        public virtual SexLookup Sex { get; set; }

        public virtual ICollection<JobSectorLookup> JobSectors { get; set; }
    }
}
