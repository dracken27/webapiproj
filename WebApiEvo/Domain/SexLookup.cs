﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class SexLookup
    {
        [Key]
        public int SexId { get; set; }
        public string Sex { get; set; }
    }
}
