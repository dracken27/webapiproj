﻿using Domain;
using Manager.Model;
using AutoMapper;


namespace WebApi
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<Project, ProjectInfo>();
            Mapper.CreateMap<Project, ProjectAreaInfo>();
            Mapper.CreateMap<ProjectInput, Project>();

            Mapper.CreateMap<Sector, SectorInfo>();
            Mapper.CreateMap<SectorInput, Sector>();

            Mapper.CreateMap<ActionArea, ActionAreaProjectInfo>();
            Mapper.CreateMap<ActionArea, ActionAreaInfo>();
            Mapper.CreateMap<ActionAreaInput, ActionArea>();

            Mapper.CreateMap<Beneficiary, BeneficiaryInfo>();
            Mapper.CreateMap<AddressInput, Address>();
            Mapper.CreateMap<BeneficiaryInput, Beneficiary>();

            Mapper.CreateMap<JobSectorLookup, JobSectorInfo>();
        }
    }
}