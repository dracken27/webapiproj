﻿using Autofac.Integration.WebApi;
using Manager.Model;
using Manager.Service;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("project")]
    public class ProjectController : ApiController
    {
        private ProjectService _service;

        public ProjectController(ProjectService service)
        {
            _service = service;
        }

        [Route("all")]
        [HttpGet]
        public IHttpActionResult GetAllProjects()
        {
            return Ok(_service.GetAllProjects());
        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult GetProjectById(int id)
        {
            var proj = _service.GetProjectById(id);
            if (proj == null)
            {
                return NotFound();
            }

            return Ok(proj);
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult AddProject(ProjectInput projectInput)
        {
            if (projectInput == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.AddNewProject(projectInput);
            return Ok();
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateProject(ProjectInput projectInput)
        {
            if (projectInput == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.UpdateProject(projectInput);
            return Ok();
        }
    }
}
