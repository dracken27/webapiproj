﻿using Manager.Model;
using Manager.Service;
using System.Web.Http;

namespace WebApi.Controllers
{
    [RoutePrefix("area")]
    public class ActionAreaController : ApiController
    {
        private ActionAreaService _service;

        public ActionAreaController(ActionAreaService service)
        {
            _service = service;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetAllAreas()
        {
            return Ok(_service.GetAreas());
        }

        [Route("projectareas/{projId}")]
        [HttpGet]
        public IHttpActionResult GetAreasForProject(int projId)
        {
            return Ok(_service.GetAreas(projId));
        }

        [Route("")]
        [HttpPost]
        public IHttpActionResult AddActionArea(ActionAreaInput actionAreaInput)
        {
            if (actionAreaInput == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.AddNewActionArea(actionAreaInput);
            return Ok();
        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateActionArea(ActionAreaInput actionAreaInput)
        {
            if (actionAreaInput == null || !ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _service.UpdateExistingActionArea(actionAreaInput);
            return Ok();
        }
    }
}