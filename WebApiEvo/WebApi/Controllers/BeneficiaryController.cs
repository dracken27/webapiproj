﻿using System.Web.Http;
using Manager.Model;
using Manager.Service;

namespace WebApi.Controllers
{
    [RoutePrefix("beneficiary")]
    public class BeneficiaryController : ApiController
    {
        private readonly BeneficiaryService _beneficiaryService;

        public BeneficiaryController(BeneficiaryService beneficiaryService)
        {
            _beneficiaryService = beneficiaryService;
        }

        [Route]
        public IHttpActionResult GetBeneficiaries()
        {
            return Ok(_beneficiaryService.GetAllBeneficiaries());
        }

        [Route("{id}")]
        public IHttpActionResult GetBeneficiary(int id)
        {
            var beneficiary = _beneficiaryService.GetBeneficiaryById(id);

            if (beneficiary == null)
                return NotFound();

            return Ok(beneficiary);
        }

        [Route]
        [HttpPost]
        public IHttpActionResult Add(BeneficiaryInput beneficiaryInfo)
        {
            if (beneficiaryInfo == null ||
                !ModelState.IsValid)
                return BadRequest();

            _beneficiaryService.AddBeneficiary(beneficiaryInfo);
            return Ok();
        }

        [Route]
        [HttpPut]
        public IHttpActionResult Update(BeneficiaryInput beneficiaryInfo)
        {
            if (beneficiaryInfo == null ||
                !ModelState.IsValid)
                return BadRequest();

            _beneficiaryService.UpdateBeneficiary(beneficiaryInfo);
            return Ok();
        }

        [Route("{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
           var succeded = _beneficiaryService.DeleteBeneficiary(id);
            if (!succeded)
                return NotFound();

            return Ok();
        }
    }
}
