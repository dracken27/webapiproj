﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;

namespace WebApi.Modules
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var managerAssembly = Assembly.Load("Manager");
            builder.RegisterAssemblyTypes(managerAssembly)
                   .Where(t => t.Name.EndsWith("Service"));
        }
    }
}