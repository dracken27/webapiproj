﻿using Autofac;
using Autofac.Integration.WebApi;
using Data;

namespace WebApi.Modules
{
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EvoDbContext>().InstancePerLifetimeScope();
        }
    }
}