﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;

namespace WebApi.Modules
{
    public class RepositoryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var dataAssembly = Assembly.Load("Data");
            builder.RegisterAssemblyTypes(dataAssembly)
                   .Where(t => t.Name.EndsWith("Repository"))
                   .InstancePerLifetimeScope();
        }
    }
}