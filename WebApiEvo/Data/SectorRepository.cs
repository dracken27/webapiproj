﻿using Domain;

namespace Data
{
    public class SectorRepository : GenericRepository<Sector>
    {
        public SectorRepository(EvoDbContext context) : base(context)
        {
        }
    }
}
