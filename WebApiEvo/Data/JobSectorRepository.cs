﻿using Domain;

namespace Data
{
    public class JobSectorRepository : GenericRepository<JobSectorLookup>
    {
        public JobSectorRepository(EvoDbContext context)
            : base(context)
        {
        }
    }
}
