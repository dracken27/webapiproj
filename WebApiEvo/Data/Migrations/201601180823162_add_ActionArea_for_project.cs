namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_ActionArea_for_project : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionAreas",
                c => new
                    {
                        ActionAreaId = c.Int(nullable: false, identity: true),
                        CodeName = c.String(),
                    })
                .PrimaryKey(t => t.ActionAreaId);
            
            CreateTable(
                "dbo.ActionAreaProjects",
                c => new
                    {
                        ActionArea_ActionAreaId = c.Int(nullable: false),
                        Project_ProjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ActionArea_ActionAreaId, t.Project_ProjectId })
                .ForeignKey("dbo.ActionAreas", t => t.ActionArea_ActionAreaId, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectId, cascadeDelete: true)
                .Index(t => t.ActionArea_ActionAreaId)
                .Index(t => t.Project_ProjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActionAreaProjects", "Project_ProjectId", "dbo.Projects");
            DropForeignKey("dbo.ActionAreaProjects", "ActionArea_ActionAreaId", "dbo.ActionAreas");
            DropIndex("dbo.ActionAreaProjects", new[] { "Project_ProjectId" });
            DropIndex("dbo.ActionAreaProjects", new[] { "ActionArea_ActionAreaId" });
            DropTable("dbo.ActionAreaProjects");
            DropTable("dbo.ActionAreas");
        }
    }
}
