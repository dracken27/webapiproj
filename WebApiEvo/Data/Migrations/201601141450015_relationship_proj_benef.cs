namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relationship_proj_benef : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Beneficiaries", "Project_ProjectId", c => c.Int());
            CreateIndex("dbo.Beneficiaries", "Project_ProjectId");
            AddForeignKey("dbo.Beneficiaries", "Project_ProjectId", "dbo.Projects", "ProjectId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Beneficiaries", "Project_ProjectId", "dbo.Projects");
            DropIndex("dbo.Beneficiaries", new[] { "Project_ProjectId" });
            DropColumn("dbo.Beneficiaries", "Project_ProjectId");
        }
    }
}
