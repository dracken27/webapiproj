namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class null_datetime_benefic : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Beneficiaries", "DateOfBirth", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Beneficiaries", "DateOfBirth", c => c.DateTime(nullable: false));
        }
    }
}
