namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_ActionAreaRepository : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ActionAreaProjects", newName: "ProjectActionAreas");
            DropPrimaryKey("dbo.ProjectActionAreas");
            AddPrimaryKey("dbo.ProjectActionAreas", new[] { "Project_ProjectId", "ActionArea_ActionAreaId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ProjectActionAreas");
            AddPrimaryKey("dbo.ProjectActionAreas", new[] { "ActionArea_ActionAreaId", "Project_ProjectId" });
            RenameTable(name: "dbo.ProjectActionAreas", newName: "ActionAreaProjects");
        }
    }
}
