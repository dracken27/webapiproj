namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test_3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BeneficiariesJobSectors",
                c => new
                    {
                        Beneficiary_BeneficiaryId = c.Int(nullable: false),
                        JobSectorLookup_JobSectorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Beneficiary_BeneficiaryId, t.JobSectorLookup_JobSectorId })
                .ForeignKey("dbo.Beneficiaries", t => t.Beneficiary_BeneficiaryId, cascadeDelete: true)
                .ForeignKey("dbo.JobSectorLookups", t => t.JobSectorLookup_JobSectorId, cascadeDelete: true)
                .Index(t => t.Beneficiary_BeneficiaryId)
                .Index(t => t.JobSectorLookup_JobSectorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BeneficiariesJobSectors", "JobSectorLookup_JobSectorId", "dbo.JobSectorLookups");
            DropForeignKey("dbo.BeneficiariesJobSectors", "Beneficiary_BeneficiaryId", "dbo.Beneficiaries");
            DropIndex("dbo.BeneficiariesJobSectors", new[] { "JobSectorLookup_JobSectorId" });
            DropIndex("dbo.BeneficiariesJobSectors", new[] { "Beneficiary_BeneficiaryId" });
            DropTable("dbo.BeneficiariesJobSectors");
        }
    }
}
