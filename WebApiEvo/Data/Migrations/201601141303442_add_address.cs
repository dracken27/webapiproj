namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_address : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        City = c.String(),
                        Street = c.String(),
                        Number = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AddressId);
            
            AddColumn("dbo.Beneficiaries", "AddressId", c => c.Int(nullable: false));
            CreateIndex("dbo.Beneficiaries", "AddressId");
            AddForeignKey("dbo.Beneficiaries", "AddressId", "dbo.Addresses", "AddressId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Beneficiaries", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Beneficiaries", new[] { "AddressId" });
            DropColumn("dbo.Beneficiaries", "AddressId");
            DropTable("dbo.Addresses");
        }
    }
}
