namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test_2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SexLookups",
                c => new
                    {
                        SexId = c.Int(nullable: false, identity: true),
                        Sex = c.String(),
                    })
                .PrimaryKey(t => t.SexId);
            
            CreateTable(
                "dbo.JobSectorLookups",
                c => new
                    {
                        JobSectorId = c.Int(nullable: false, identity: true),
                        JobSector = c.String(),
                    })
                .PrimaryKey(t => t.JobSectorId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        ProjTheme = c.Int(nullable: false),
                        ProjSector_SectorId = c.Int(),
                    })
                .PrimaryKey(t => t.ProjectId)
                .ForeignKey("dbo.Sectors", t => t.ProjSector_SectorId)
                .Index(t => t.ProjSector_SectorId);
            
            CreateTable(
                "dbo.Sectors",
                c => new
                    {
                        SectorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SectorId);
            
            AddColumn("dbo.Beneficiaries", "SexId", c => c.Int(nullable: false));
            CreateIndex("dbo.Beneficiaries", "SexId");
            AddForeignKey("dbo.Beneficiaries", "SexId", "dbo.SexLookups", "SexId", cascadeDelete: true);
            DropColumn("dbo.Beneficiaries", "Sex_SexId");
            DropColumn("dbo.Beneficiaries", "Sex_Sex");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Beneficiaries", "Sex_Sex", c => c.String());
            AddColumn("dbo.Beneficiaries", "Sex_SexId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Projects", "ProjSector_SectorId", "dbo.Sectors");
            DropForeignKey("dbo.Beneficiaries", "SexId", "dbo.SexLookups");
            DropIndex("dbo.Projects", new[] { "ProjSector_SectorId" });
            DropIndex("dbo.Beneficiaries", new[] { "SexId" });
            DropColumn("dbo.Beneficiaries", "SexId");
            DropTable("dbo.Sectors");
            DropTable("dbo.Projects");
            DropTable("dbo.JobSectorLookups");
            DropTable("dbo.SexLookups");
        }
    }
}
