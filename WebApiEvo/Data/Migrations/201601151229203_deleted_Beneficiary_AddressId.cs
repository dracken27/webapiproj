namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deleted_Beneficiary_AddressId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Beneficiaries", "AddressId", "dbo.Addresses");
            DropIndex("dbo.Beneficiaries", new[] { "AddressId" });
            RenameColumn(table: "dbo.Beneficiaries", name: "AddressId", newName: "Address_AddressId");
            AlterColumn("dbo.Beneficiaries", "Address_AddressId", c => c.Int());
            CreateIndex("dbo.Beneficiaries", "Address_AddressId");
            AddForeignKey("dbo.Beneficiaries", "Address_AddressId", "dbo.Addresses", "AddressId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Beneficiaries", "Address_AddressId", "dbo.Addresses");
            DropIndex("dbo.Beneficiaries", new[] { "Address_AddressId" });
            AlterColumn("dbo.Beneficiaries", "Address_AddressId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Beneficiaries", name: "Address_AddressId", newName: "AddressId");
            CreateIndex("dbo.Beneficiaries", "AddressId");
            AddForeignKey("dbo.Beneficiaries", "AddressId", "dbo.Addresses", "AddressId", cascadeDelete: true);
        }
    }
}
