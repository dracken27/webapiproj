using Domain;

namespace Data.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<EvoDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EvoDbContext context)
        {
            context.JobSector.AddOrUpdate(
                new JobSectorLookup { JobSectorId = 1, JobSector = "One"},
                new JobSectorLookup { JobSectorId = 2, JobSector = "Two"},
                new JobSectorLookup { JobSectorId = 3, JobSector = "Three"});

            context.Sex.AddOrUpdate(
                new SexLookup{SexId = 1, Sex = "Male"},
                new SexLookup{SexId = 2, Sex = "Female"},
                new SexLookup{SexId = 3, Sex = "Alien"},
                new SexLookup{SexId = 4, Sex = "Other"});


        }
    }
}
