// <auto-generated />
namespace Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class deleted_Beneficiary_AddressId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(deleted_Beneficiary_AddressId));
        
        string IMigrationMetadata.Id
        {
            get { return "201601151229203_deleted_Beneficiary_AddressId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
