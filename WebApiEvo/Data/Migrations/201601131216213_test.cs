namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Beneficiaries",
                c => new
                    {
                        BeneficiaryId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Sex_SexId = c.Int(nullable: false),
                        Sex_Sex = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        ProfileStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BeneficiaryId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Beneficiaries");
        }
    }
}
