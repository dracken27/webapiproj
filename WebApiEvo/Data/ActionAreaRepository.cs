﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;

namespace Data
{
    public class ActionAreaRepository : GenericRepository<ActionArea>
    {
        public ActionAreaRepository(EvoDbContext context) : base(context)
        {
        }

        public override IEnumerable<ActionArea> GetAll()
        {
            return DbSet.Include(a => a.Projects).AsEnumerable();
        }

        public override ActionArea GetById(int id)
        {
            return DbSet.Include(a => a.Projects).SingleOrDefault(x => x.ActionAreaId == id);
        }
    }
}
