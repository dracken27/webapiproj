﻿using System.Data.Entity;
using Domain;

namespace Data
{
    public class EvoDbContext : DbContext
    {
        public EvoDbContext() : base("EVO"){}

        public DbSet<Beneficiary> Beneficiaries { get; set; }
        public DbSet<SexLookup> Sex { get; set; }
        public DbSet<JobSectorLookup> JobSector { get; set; }
        public DbSet<Address> Addresses { get; set; }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Sector> Sectors { get; set; }
        public DbSet<ActionArea> ActionAreas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Beneficiary>()
                .HasMany(x => x.JobSectors)
                .WithMany(x => x.Beneficiaries)
                .Map(x => x.ToTable("BeneficiariesJobSectors"));
        }
    }
}
