﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Data
{
    public abstract class GenericRepository<T> where T : class
    {
        protected EvoDbContext Context;
        protected readonly IDbSet<T> DbSet;

        protected GenericRepository(EvoDbContext context)
        {
            Context = context;
            DbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbSet.AsEnumerable();
        }

        public virtual T GetById(int id)
        {
            return DbSet.Find(id);
        }

        public virtual void Add(T entity)
        {
            DbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            DbSet.Remove(entity);
        }
    }
}
