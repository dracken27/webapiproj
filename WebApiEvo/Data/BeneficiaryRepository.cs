﻿using System.Data.Entity;
using System.Linq;
using Domain;

namespace Data
{
    public class BeneficiaryRepository : GenericRepository<Beneficiary>
    {
        public BeneficiaryRepository(EvoDbContext context)
            : base(context)
        {
        }

        public override Beneficiary GetById(int id)
        {
            return DbSet.Include(x => x.Address).Include(x => x.JobSectors).FirstOrDefault(x => x.BeneficiaryId == id);
        }
    }
}
