﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain;

namespace Data
{
    public class ProjectRepository : GenericRepository<Project>
    {
        public ProjectRepository(EvoDbContext context)
            : base(context)
        {
        }

        public override Project GetById(int id)
        {
            return Context.Projects
                .Include(p => p.ProjSector)
                .Include(p => p.Areas)
                .SingleOrDefault(p => p.ProjectId == id);
        }
    }
}
